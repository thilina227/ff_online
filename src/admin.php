<?php
// if ($_SESSION['role'] == "ADMIN") {
// //do nothing
// }else{
// header('Location: index.php');
// }
?>
<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section class="row full-width">
			
			<div class="large-3 columns" style="text-align: center;">
				<p></p>
			</div>
			<div id="panel" class="large-6 columns" style="text-align: center;">
				<a class="tile" href="orders.php">Orders</a>
				<a class="tile" href="items.php">Items</a>
				<a class="tile" href="add_item.php">Add Item</a>
			</div>
			<div class="large-3 columns" style="text-align: center;">
				<p></p>
			</div>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
	</body>
	<style type="text/css" media="screen">
		.tile{
			margin: 20px;
			display: inline-block;
			border: 1px solid #fff;
			width: 30%;
			min-height: 50px;
			padding: 20px;
		}
		#panel{
			min-height: 350px;
			padding-top: 30px;
		}
	</style>
</html>
