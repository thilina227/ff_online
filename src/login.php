<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section class="row full-width">
			<form id="login_form" action="#" method="post" accept-charset="utf-8">
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<h2>Login</h2>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="email" required="true" name="email" placeholder="Email" value="" id="email"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="password" name="password" placeholder="Password" value="" id="password"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<br />
					</div>
					<div class="large-4 medium-4 columns end">
						<input id="btn_login" class="button secondary radius" type="submit" value="Login"/>
					</div>
				</div>
			</form>
		</section>
		<!-- footer -->
		<?php
			include 'template/footer.php';
		?>
	</body>
	<style type="text/css" media="screen">
		#login_form{
			margin: 20px;
			min-height: 350px;
		}
	</style>
	<script type="text/javascript" charset="utf-8">
			var request;
			$(document).ready(function() {
				
				$("#btn_login").click(function() {
					$(".error").hide();
					if(validate()) {
						submitForm();
					}
					return false;
				});
				
			});
						
			$.fn.serializeObject = function(){//serialize form data
			    var o = {};
			    var a = this.serializeArray();
			    $.each(a, function() {
			        if (o[this.name] !== undefined) {
			            if (!o[this.name].push) {
			                o[this.name] = [o[this.name]];
			            }
			            o[this.name].push(this.value || '');
			        } else {
			            o[this.name] = this.value || '';
			        }
			    });
			    return o;
			};						

			//ajax submit form data
			function submitForm(){
				var values = $("#login_form").serializeObject();
				 $.ajax({
			        url: "login_controller.php",
			        type: "post",
			        data: values,
			        success: function(result){
			            var obj = $.parseJSON(result);
			            if(obj['status'] == "SUCCESS"){
			            	$("#btn_login").after('<small style="margin-left:20px;" class="success">'+obj['message']+'</small>');
			            	window.location.replace('index.php');
			            }else{
			            	$("#btn_login").before('<small class="error">'+obj['message']+'</small>');
			            }
			        },
			        error:function(result){
			            alert(result);
			        }
			    });
			};			
			
			function validate() {//client side validation user inputs
				var email = $("#email").val();
				var password = $("#password").val();

				if(email == ""){
					$("#email").after('<small class="error">Required.</small>');
					return false;
				}
				if(password == ""){
					$("#password").after('<small class="error">Required.</small>');
					return false;
				}
				
				return true;
			};
		</script>
</html>
