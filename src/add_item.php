<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	require_once 'bootstrap.php';
	require_once 'orm/dao/item_dao.php';
	require_once 'orm/dao/category_dao.php';
	require_once 'orm/dao/sub_category_dao.php';
	
	use model\Item;
	use model\Category;
	use model\SubCategory;
	use dao\ItemDao;
	use dao\CategoryDao;
	use dao\SubCategoryDao;
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section class="row full-width">
			<form id="item_form" action="add_item_controller.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<h2>Add Item</h2>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<?php if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
								echo "<h3 class=\"success\">".$_SESSION['message']."</h3>";
								$_SESSION['message'] = "";
							}
						?>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Item name
						<input type="text" required="true" name="name" placeholder="Name" value="" id="name"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Type
						<br/>
						<select class="large-4 medium-4 columns end" style="width: 100%;" id="type" name="type">
							<option value="FOOD"> Food </option>
							<option value="BEAVERAGE"> Beaverage </option>
						</select>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Category
						<br/>
						<select class="large-4 medium-4 columns end" style="width: 100%;" onchange="loadSubCategories();" id="category" name="category">
							<option value="0">--Select--</option>
							<?php 
								$categoryDao = new CategoryDao($entityManager);
								$categories = $categoryDao->getall();
								foreach ($categories as $key => $cagetory) {
									echo "<option value=\"".$cagetory->getId()."\">".$cagetory->getName()."</option>";
								}
							 ?>
						</select>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Sub Category
						<br/>
						<select class="large-4 medium-4 columns end" style="width: 100%;" id="subCategory" name="subCategory">
							<option id="default_sub_category" value="0">--Select--</option>
						</select>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Price
						<input type="number" required="true" name="price" placeholder="Price" value="" id="price"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Image
						<img id="uploadPreview" src="images/no_image.jpg" /><br />
						<input required="true" id="image" type="file" name="image" onchange="PreviewImage();" />
					</div>
				</div>
				
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<br />
					</div>
					<div class="large-4 medium-4 columns end">
						<input id="btn_add" class="button secondary radius" type="submit" value="Add"/>
					</div>
				</div>
			</form>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
	</body>
	<style type="text/css" media="screen">
		.lbl {
			text-align: right;
			padding-top: 10px;
			color: #FFFFFF;
		}
		#signup-form {
			margin: 20px;
		}

	</style>
	<script type="text/javascript" charset="utf-8">
		
	function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    }
		
		$(document).ready(function() {

			$("#btn_add").click(function() {
				$(".error").hide();
				$(".success").hide();
				if(validate()) {
					submitForm();
				}
				return false;
			});
		});
			
			
		//ajax submit form data
		function submitForm() {
			$("#item_form").submit();
			// var values = $("#item_form").serializeObject();
			// $.ajax({
				// url : "add_item_controller.php",
				// type : "post",
				// data : values,
				// dataType: 'json',
				// success : function(result) {
					// alert(result);
					// var obj = $.parseJSON(result);
					// if(obj['status'] == "SUCCESS") {
						// $("#btn_add").after('<small style="margin-left:20px;" class="success">' + obj['message'] + '</small>');
						// $("#name").val("");
						// $("#price").val("");
					// } else {
						// $("#btn_add").before('<small class="error">An error occurred, please try again later</small>');
					// }
					// console.log(result)
				// },
				// error : function(result) {
					// alert(result);
					// console.log(result)
				// }
			// });
		};

		function loadSubCategories() {
			$(".sub_category").hide();
			var category = $("#category").val();
			$.ajax({
				url : "sub_category_controller.php",
				type : "post",
				data : {'category':category},
				success : function(result) {
					var obj = $.parseJSON(result);
					if(obj['status'] == "SUCCESS") {
						obj['subCategories'].forEach(function (subCategory){
							$('#default_sub_category').after('<option class="sub_category" value="'+subCategory['id']+'">'+subCategory['name']+'</option>');
						});
					}
					console.log(result)
				},
				error : function(result) {
					alert(result);
					console.log(result)
				}
			});
		};


		$.fn.serializeObject = function() {//serialize form data
			var o = {};
			var a = this.serializeArray();
			$.each(a, function() {
				if(o[this.name] !== undefined) {
					if(!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});
			return o;
		};
		function validate() {//client side validation user inputs
			var itemname = $("#name").val();
			var price = $("#price").val();
			var category = $("#category").val();
			var subCategory = $("#subCategory").val();
			var image = $("#image").val();

			if(itemname == "") {
				$("#name").after('<small class="error">Required.</small>');
				return false;
			}
			
			if(category == 0){
				$("#category").after('<small class="error">Required.</small>');
				return false;
			}
			
			if(subCategory == 0){
				$("#subCategory").after('<small class="error">Required.</small>');
				return false;
			}
			
			if(price == "") {
				$("#price").after('<small class="error">Required.</small>');
				return false;
			}
			
			if(image == "") {
				$("#image").after('<small class="error">Required.</small>');
				return false;
			}
			
			return true;
		};
	</script>
</html>
