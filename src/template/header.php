<?php 
session_start();
 ?>
<header id="header" class="row full-width">
	<div id="mobile-header" style="padding: 10px; background-color: #333;">
		<a id="responsive-menu-button" href="#sidr-main"><img src="images/menu.png" style="width: 30px; height: 30px;"/></a>
	</div>
	<div class="large-8 columns">
		<div class="row">
			<div class="large-6 columns"></div>
			<div class="large-6 columns">
				<img src="images/banner.png" alt="Fast Food Online" style="max-height: 120px; width: 100%; height: 100%;" align="center"/>
			</div>
		</div>
		<div class="row">
			<div align="center" id="social_links" class="large-6 columns" style="margin-top: -5px">
				Follow us on: <a href="#"><img src="images/icon_fb.png" alt="Facebook"/></a>
				<a href="#"><img src="images/icon_twitter.png" alt="twitter"/></a>
				<a href="#"><img src="images/icon_pinterest.png" alt="Pinterest"/></a>
				<a href="#"><img src="images/icon_youtube.png" alt="Youtube"/></a>
			</div>
			<div align="center" class="large-6 columns">
				<span style="color: #00eb00; font-size: 1.6em; "> Your food is here</span>
			</div>
		</div>
	</div>
	<div class="large-4 columns">
		<div class="large-4 columns">
			<?php 
				if(!isset($_SESSION['token'])){
			 ?>
			 <a href="signup.php">Sign up</a>
			<?php }else { ?>
				 	<a href="myaccount.php">My Account</a>
			<?php } ?>
		</div>
		<div class="large-3 columns end">
			<?php if(!isset($_SESSION['token'])){ ?>
			<a href="login.php">Login</a>
			<?php }else { ?>
			<a href="logout.php">Logout</a>
			<?php } ?>
		</div>
		<img src="images/hotline.png" height="50%" style="max-height: 110px; max-width: 300px;" width="60%" align="right" alt="Hot Line" />
	</div>
</header>