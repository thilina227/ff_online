<style>
	#mobile-header {
		display: none;
	}
	@media only screen and (max-width: 767px) {
		#mobile-header {
			display: block;
		}
		#navigation {
			display: none;
		}
	}	.nav {
		border-bottom: 2px solid #3a1219;
		border-top: 2px solid #3a1219;
		background-color: #791324;
	}
	#navigation ul {
		margin: 0px;
		display: table;
		width: 100%;
		padding: 0px;
	}
	#navigation li {
		width: 16%;
		display: table-cell;
		text-align: justify;
	}
	#navigation li a {
		display: block;
		text-align: center;
		border: 2px solid #e2415d;
		text-decoration: none;
		border-radius: 5px;
		padding: 10px;
	}
	.colspace{
		display: inline-block;
		width: 2%;
	}
	.nav-active{
		background-color: #e13e58;
		color: #404040;
	}
	.nav_default{
		color: #FFFFFF;
		background-color: #ec8798;
	}
</style>
<div id="navigation">
	<nav class="row full-width nav">
		<ul>
			<li>
				<?php 
					if(isset($_SESSION['role']) && $_SESSION['role']== "ADMIN"){
				 ?>
					<a href="admin.php" class="nav_default">Dashboard</a>				
				<?php 
					}else{
				 ?>
				 	<a href="index.php" class="nav-active">Home</a>
				 <?php } ?>
				
			</li>
			<div class="colspace"></div>
			<li>
				<?php 
					if(isset($_SESSION['role']) && $_SESSION['role']== "ADMIN"){
				 ?>
					<a href="orders.php" class="nav_default">Orders</a>				
				<?php 
					}else{
				 ?>
				 	<a href="search.php?type=FOOD" class="nav_default">Food</a>
				 <?php } ?>
			</li>
			<div class="colspace"></div>
			<li>
				<?php 
					if(isset($_SESSION['role']) && $_SESSION['role']== "ADMIN"){
				 ?>
				<a href="items.php" class="nav_default">Items</a>
				<?php 
					}else{
				 ?>
				<a href="search.php?type=BEAVERAGE" class="nav_default">Beaverage</a> 
				 <?php } ?>
				
			</li>
			<div class="colspace"></div>
			<li>
				<a href="contact.php" class="nav_default">Contact us</a>
			</li>
			<div class="colspace"></div>
			<li>
				<a href="mycart.php" class="nav_default">My Cart</a>
			</li>
		</ul>
	</nav>
</div>