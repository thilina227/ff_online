<footer>
	<div class="row full-width" id="icons_bar">
		<div class="large-4 medium-4 columns footer-col" style="text-align: center">
			<img src="images/icon_call.png" alt="Order Online" />
			<div class="bottom_bar">
				Free Online Ordering
			</div>
		</div>
		<div class="large-4 medium-4 columns footer-col" style="text-align: center">
			<img src="images/icon_menu.png" alt="Menus" />
			<div class="bottom_bar">
				Thousands of Menus
			</div>
		</div>
		<div class="large-4 medium-4 columns footer-col" style="text-align: center">
			<img src="images/icon_dollar.png" alt="Discount" />
			<div class="bottom_bar">
				Discounts and Details
			</div>
		</div>
	</div>
</footer>
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<link rel="stylesheet" href="css/jquery.sidr.dark.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
<script src="js/jquery.sidr.min.js" type="text/javascript" charset="utf-8"></script>
<script>
	$('#responsive-menu-button').sidr({
		name : 'sidr-main',
		source : '#navigation'
	});

</script>
<script>
	$(document).foundation();

</script>
<script>
	$('#responsive-menu-button').sidr({
		name : 'sidr-main',
		source : '#navigation'
	});

</script>
<style type="text/css" media="screen">
	#social_links img {
		width: 40px;
		height: 40px;
	}
	#icons_bar img {
		width: 60px;
		height: 60px;
		margin-bottom: -20px;
	}
	.bottom_bar {
		color: #000000;
		background: -webkit-linear-gradient(#d1d1d1, #828282); /* For Safari 5.1 to 6.0 */
		background: -o-linear-gradient(#d1d1d1, #828282); /* For Opera 11.1 to 12.0 */
		background: -moz-linear-gradient(#d1d1d1, #828282); /* For Firefox 3.6 to 15 */
		background: linear-gradient(#d1d1d1, #828282); /* Standard syntax (must be last) */
		border: 1px solid #000000;
		padding: 20px;
	}
	.footer-col {
		padding: 0px;
	}

</style>