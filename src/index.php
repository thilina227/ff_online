<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
		include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section>
			<div class="row" >
			  	<div class="large-12 columns" style="text-align: center; z-index: 99999;">
					<form action="search.php" method="get" id="search_form" accept-charset="utf-8">
						<div class="row collapse postfix-round">
							<div class="small-3 columns">
								<label style="text-align: center; padding-left: 10px; vertical-align: middle; display: table-cell; margin: 0px; font-size: large; height: 50px;" for="search_box">What would you like:</label>
							</div>
							<div class="small-7 columns">
								<input name="search" id="search_box" type="text" placeholder="Search..." style="margin: 0px; height: 50px;"/>
							</div>
							<div class="small-2 columns">
								<div id="btn_search" class="postfix">
								  <img src="images/search.png" alt="Search" style="height: 50px; padding: 5px;"/>
								</div>
							</div>
						</div>
					</form>					
				</div>
			</div>
			<div class="row full-width full-height">
				<div class="large-1 columns"><br/></div>
				<div id="section" class="large-10 columns full-height">
					<div id="bulletLooper" data-looper="go" class="looper slide full-height">
						<div class="looper-inner">
							<div class="item">
								<img src="images/carousel/1.jpg" alt="">
							</div>
							<div class="item">
								<img src="images/carousel/2.jpg" alt="">
							</div>
							<div class="item">
								<img src="images/carousel/3.jpg" alt="">
							</div>
							<div class="item">
								<img src="images/carousel/4.jpg" alt="">
							</div>
						</div>
						<nav>
							<a class="looper-control" data-looper="prev" href="#bulletLooper"> <img src="images/left.png" /> </a>
							<a class="looper-control right" data-looper="next" href="#bulletLooper"> <img src="images/right.png" /> </a>
						</nav>
					</div>
				</div>
				<div class="large-1 columns"><br/></div>
			</div>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>

		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$("#btn_search").click(function() {
					if(validate()) {
						// submitForm();
						$("#search_form").submit();
					}
					return false;
				});
			});
			//ajax submit form data
			function submitForm() {
				var values = $("#search_form").serializeObject();
				$.ajax({
					url : "search_controller.php",
					type : "post",
					data : values,
					success : function(result) {
						$('#bulletLooper').hide();
						var obj = $.parseJSON(result);
						alert(result);
						if(obj['status'] == "SUCCESS") {
							
						} else {
							//todo show no results
						}
						console.log(result)
					},
					error : function(result) {
						alert(result);
						console.log(result)
					}
				});
			};
	
	
			$.fn.serializeObject = function() {//serialize form data
				var o = {};
				var a = this.serializeArray();
				$.each(a, function() {
					if(o[this.name] !== undefined) {
						if(!o[this.name].push) {
							o[this.name] = [o[this.name]];
						}
						o[this.name].push(this.value || '');
					} else {
						o[this.name] = this.value || '';
					}
				});
				return o;
			};
			function validate() {//client side validation user inputs
				var search = $("#search_box").val();
				if(search == "") {
					return false;
				}
				return true;
			};
			
		</script>
		<link rel="stylesheet" href="css/looper.css" type="text/css" />
		<script src="js/looper.min.js" type="text/javascript" charset="utf-8"/>
		<script type="text/javascript" charset="utf-8">
			jQuery(function($) {
				$('#bulletLooper').on('shown', function(e) {
					$('.looper-nav > li', this).removeClass('active').eq(e.relatedIndex).addClass('active');
				});
			});
		</script>
		<style type="text/css" media="screen">
			#bulletLooper nav img{
				margin-bottom: 8px;
			}
			#bulletLooper .item img{
				width: 100%;
				height: 100%;
			  	-webkit-box-shadow: inset 0 0 100px #fff;
	     		-moz-box-shadow: inset 0 0 100px #fff;
	          	box-shadow: inset 0 0 100px #fff;
			}
			@media only screen and (max-width: 767px) {
				#bulletLooper {
					display: none;
				}
			}
			#bulletLooper{
				margin-top: 20px;
				margin-bottom: 20px;
			}
			#search_form{
				background: -webkit-linear-gradient(#f9e5e6, #ec858f); /* For Safari 5.1 to 6.0 */
				background: -o-linear-gradient(#f9e5e6, #ec858f); /* For Opera 11.1 to 12.0 */
				background: -moz-linear-gradient(#f9e5e6, #ec858f); /* For Firefox 3.6 to 15 */
				background: linear-gradient(#f9e5e6, #ec858f); /* Standard syntax (must be last) */
				border: 1px solid #e03754;
				padding: 0px;
				border-radius: 10px;
				color:#000000;
				font-size: large;
				margin-top:20px;
				margin-bottom: -50px;
				z-index: 99999;
				display: block;
				width: inherit;
			}
			#btn_search{
				border: none;
				height: 50px;
				border-left: 1px solid #e03754;
				cursor: pointer;
			}
			#search_box{
				background-color: transparent;
				border: none;
				font-size: large;
			}
			#section{
				min-height: 350px;
			}
		</style>
	</body>
</html>
