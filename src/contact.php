<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section id="contact" class="row full-width">
			<div class="large-6 columns" style="text-align: center;">
				<h2>
					Contact us: +94 11 4 878 085<br/>
            		Email: Fastfood@gmail.com
				</h2>
				<form action="#" id="contact_form" method="post" accept-charset="utf-8">
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="feedback">Feedback</label>
						</div>
						<div class="large-8 medium-8 columns">
							<textarea name="feedback" rows="8" cols="40">
								
							</textarea>
						</div>
					</div>
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="phone">Contact number</label>
						</div>
						<div class="large-8 medium-8 columns ">
							<input type="text" required="true" type="number" name="phone" placeholder="Phone number" value="" id="phone"/>
						</div>
					</div>
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="email">Email</label>
						</div>
						<div class="large-8 medium-8 columns ">
							<input type="email" name="email" placeholder="Email" value="" id="email"/>
						</div>
					</div>
					
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<br />
						</div>
						<div class="large-8 medium-8 columns">
							<input id="btn-signup" class="button secondary radius" type="submit" value="Submit"/>
						</div>
					</div>
				</form>
			</div>
			<div class="large-6 columns" style="text-align: center;">
				<img src="images/delivery_banner.png" />
				<div class="row">
				  <div class="large-4 columns">
					Fast Food<br/>
					No.321/A,<br/>
					Union Place,<br/>
					Colombo-02.<br />
					Tel: 112-555-530
				  </div>
				  <div class="large-4 columns">
					Fast Food <br />
					No.531,<br />
					Madiwela Rd,<br />
					Thalawathugoda.<br />
					Tel: 116-879-678
				  </div>
				  <div class="large-4 columns">
					Fast Food <br />
					No.6<br />
					D.S.Senanayake Road<br />
					Kandy<br />
					Tel: 815-877-896
				  </div>
				</div>
				<div class="row">
					<br /><br />
				  <div class="large-4 columns">
					Fast Food<br />
					No.168,<br />
					Colombo Road,<br />	
					Negombo.<br />
					Tel: 315-678-678
				  </div>
				  <div class="large-4 columns">
					Fast Food <br />
					No.317, Kandy Rd,<br />
					Kurunagala.<br />
					Tel: 377-566-566
				  </div>
				  <div class="large-4 columns">
					Fast Food <br />
					No.170A, <br />
					Negombo Rd<br />
					Ja-Ela.<br />
					Tel: 113-567-566
				  </div>
				</div>
				<div class="row">
				  <div class="large-4 columns">
					
				  </div>
				  <div class="large-4 columns">
					
				  </div>
				  <div class="large-4 columns">
					
				  </div>
				  
				</div>
			</div>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
	</body>
	<style type="text/css" media="screen">
		#contact {
			min-height: 350px;
			margin: 20px;
		}
		.lbl {
			text-align: right;
			padding-top: 10px;
			color: #FFFFFF;
		}
		#contact_form {
			margin: 20px;
		}
	</style>
</html>
