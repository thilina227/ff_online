<?php 
	require_once 'bootstrap.php';
	require_once 'orm/dao/item_dao.php';
	require_once 'orm/dao/category_dao.php';
	require_once 'orm/dao/cart_dao.php';
	require_once 'orm/dao/user_dao.php';
	require_once 'orm/dao/cart_item_dao.php';
	require_once 'orm/dao/sub_category_dao.php';
	
	use dao\UserDao;
	use dao\CartDao;
	use dao\CartItemDao;
	use model\Category;
	use model\SubCategory;
	use dao\ItemDao;
	use dao\CategoryDao;
	use dao\SubCategoryDao;
?>
<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section id="section" class="row full-width">
			<div class="large-2 columns">
				<p></p>
			</div>
			<div class="large-8 columns">
				<h2>Your cart:</h2>
				<table id="tbl_cart">
					<thead>
						<tr>
							<th >#</th>
							<th >Item name</th>
							<th >Price of item</th>
							<th >Quantity</th>
							<th >Price (Rs)</th>
							<th >Operation</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$cartDao = new CartDao($entityManager);
							$cartItemDao = new CartItemDao($entityManager);
							$userDao = new UserDao($entityManager);		
							$total = 0;			
							
							if(isset($_SESSION['token'])){
								$user = $userDao->findByEmail($_SESSION['token']);
								if(!empty($user)){
									$user = $user[0];
									$cart = $cartDao->findByUser($user);
									if(!empty($cart)){
										$cart = $cart[0];
										$cartItems = $cartItemDao->findByCart($cart);
										if(!empty($cartItems)){
											$i = 1;
											foreach ($cartItems as $key => $cartItem) {
												$total = $total + ($cartItem->getItem()->getPrice()*$cartItem->getQty());
												echo "<tr>";
												echo "<td>$i</td>";
												echo "<td>".$cartItem->getItem()->getName()."</td>";
												echo "<td>".$cartItem->getItem()->getPrice()."</td>";
												echo "<td>".$cartItem->getQty()."</td>";
												echo "<td>".number_format($cartItem->getItem()->getPrice()*$cartItem->getQty(), 2)."</td>";
												echo "<td><a href=\"#\" onclick=\"removeFromCart(".$cartItem->getId().")\">remove</a><td>";
												echo "</tr>";
												$i++;
											}
										}
									}
								}
							}
						 ?>
						 <tr>
							<th ></th>
							<th ></th>
							<th ></th>
							<th >Total</th>
							<th ><?php echo number_format($total,2);?></th>
							<th ></th>
						</tr>
					</tbody>
				</table>
				<div id="btn_checkout">
				  	<a href="checkout.php">Continue</a>
				</div>
			</div>
			<div class="large-2 columns">
				<p></p>
			</div>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
	</body>
	<script type="text/javascript" charset="utf-8">
		function removeFromCart(id){
			$.ajax({
				url : "cart_controller.php",
				type : "post",
				data : {"action":"delete","id":id},
				success : function(result) {
					var obj = $.parseJSON(result);
					if(obj['status'] == "SUCCESS") {
						alert(obj['message']);
						location.reload();
					} else {
						location.reload();
					}
				},
				error : function(result) {
					alert(result);
					// console.log(result)
				}
			});
		}
	</script>
	<style type="text/css" media="screen">
		#section{
			min-height: 390px;
		}
		#tbl_cart{
			width: 100%;
		}
		#btn_checkout{
			text-align: right;
		}
		#btn_checkout a{
			border: 2px solid #e03754;
			color: #fff;
			text-decoration:none;
			background-color: #791324;
			border-radius: 5px;
			padding: 10px;
			min-width: 70px;
		}
		
	</style>
</html>
