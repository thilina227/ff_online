<?php
require_once 'bootstrap.php';
require_once 'orm/dao/item_dao.php';
require_once 'orm/dao/category_dao.php';
require_once 'orm/dao/cart_dao.php';
require_once 'orm/dao/user_dao.php';
require_once 'orm/dao/cart_item_dao.php';
require_once 'orm/dao/sub_category_dao.php';

use dao\UserDao;
use dao\CartDao;
use dao\CartItemDao;
use model\Category;
use model\SubCategory;
use dao\ItemDao;
use dao\CategoryDao;
use dao\SubCategoryDao;
?>
<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section id="section" class="row full-width">
			<div class="large-2 columns">
				<p></p>
			</div>
			<div class="large-8 columns">
				<h2>Checkout:</h2>
				<div class="row">
					<div class="large-4 medium-4 columns">
					  <br />
					</div>
				  <div class="large-4 medium-4 columns end">
						<?php if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
								echo "<h3 class=\"success\">".$_SESSION['message']."</h3>";
								$_SESSION['message'] = "";
							}
						?>
				</div>
				</div>
				
				<form action="checkout_controller.php" id="checkout_form" method="post" accept-charset="utf-8">
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="address">Delivery address:</label>
						</div>
						<div class="large-4 medium-4 columns end">
							<textarea name="address" cols="200" rows="5"></textarea>
						</div>
					</div>
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="phone">Contact number:</label>
						</div>
						<div class="large-4 medium-4 columns end">
							<input type="text" required="true" type="number" name="phone" placeholder="Contact number" value="" id="phone"/>
						</div>
					</div>
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="phone">Delivery date and time:</label>
						</div>
						<div class="large-4 medium-4 columns end">
							<input type="text" name="date" value="" data-field="datetime" placeholder="Date : Time" readonly id="date"/>
							<div id="dtBox"></div>
						</div>
						<div  class="large-4 medium-4 columns">
						  	<span style="color: red;">Remember: </span> If you do not set the date and time, your order will deliver within 45 minutes to your location.  
						</div>
					</div>
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<br />
						</div>
						<div class="large-4 medium-4 columns end">
							<input id="btn_checkout" class="button secondary radius" type="submit" value="Checkout"/>
						</div>
					</div>
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="phone">Payment Options:</label>
						</div>
						<div class="large-4 medium-4 columns end">
							<img src="images/pay,emt.png" />
						</div>
					</div>
				</form>
			</div>
			<div class="large-2 columns">
				<p></p>
			</div>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
	</body>
	<script src="js/DateTimePicker.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="css/DateTimePicker.css"/>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$("#dtBox").DateTimePicker();
			
			$("#btn_checkout").click(function() {
				$(".error").hide();
				if(validate()) {
					$("#checkout_form").submit();
				}
				return false;
			});
			
		});
		
		function validate() {//client side validation user inputs
				var phone = $("#phone").val();
				var address = $("#address").val();

				var paternPhoneno1 = /^(\+94)([0-9]{9})$/;
				var paternPhoneno2 = /^(94)([0-9]{9})$/;
				var paternPhoneno3 = /^(0)?([0-9]{9})$/;
				if(phone == ""){
					$("#phone").after('<small class="error">Required</small>');
					return false;
				}
				if(phone.match(paternPhoneno1) || phone.match(paternPhoneno2) || phone.match(paternPhoneno3)){
					//matches do nothing
				}else{
					$("#phone").after('<small class="error">Phone number is invalid. (eg: +94123456789/0123456789)</small>');
					return false;
				}
				
				if(address == ""){
					$("#address").after('<small class="error">Required.</small>');
					return false;
				}
				
				return true;
			};
		
	</script>
	<style type="text/css" media="screen">
		#section {
			min-height: 390px;
		}
		.lbl {
			text-align: right;
			padding-top: 10px;
			color: #FFFFFF;
		}
		#checkout-form {
			margin: 20px;
		}
	</style>
</html>
