<?php 
require_once 'bootstrap.php';
require_once 'orm/dao/order_dao.php';

use dao\OrderDao;
?>
<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section id="section" class="row full-width">
			<div class="large-2 columns">
				<p></p>
			</div>
			<div class="large-8 columns">
				<h2>Orders</h2>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<?php if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
								echo "<h3 class=\"success\">".$_SESSION['message']."</h3>";
								$_SESSION['message'] = "";
							}
						?>
					</div>
				</div>
				<table style="width: 100%;">
					<thead>
						<tr>
							<th>#</th>
							<th>Order date</th>
							<th>Status</th>
							<th>Total</th>
							<th>Update</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$orderDao = new OrderDao($entityManager);
							$orders = $orderDao->getAllPending();
							foreach ($orders as $key => $order) {
								//print table row
								$orderDate = $order->getOrderDate();
								if(empty($orderDate)){
									print "<tr><td>".$order->getId()."</td><td></td><td>".$order->getStatus()."</td><td>".$order->getTotal()."</td><td></td><td><a class=\"delete\" href=\"edit_order.php?id=".$order->getId()."\">update</a></td></tr>";	
								}else{
									print "<tr><td>".$order->getId()."</td><td>".$orderDate->format('Y-m-d H:i:s')."</td><td>".$order->getStatus()."</td><td>".$order->getTotal()."</td><td><a class=\"delete\" href=\"edit_order.php?id=".$order->getId()."\">update</a></td></tr>";									
								}
							}
						 ?>
					</tbody>
				</table>
			</div>
			<div class="large-2 columns">
				<p></p>
			</div>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
	</body>
	<script type="text/javascript" charset="utf-8">
		
	</script>
	<style type="text/css" media="screen">
		#section{
			min-height: 390px;
		}
	</style>
</html>
