<?php
session_start();
//signup_controller.php
//controller to handle request from signup.php 
require_once 'bootstrap.php';
require_once 'orm/dao/user_dao.php';

use model\User;
use dao\UserDao;
	if($_POST){
		$email = $_POST['email'];
		$password = $_POST['password'];
		
		$userDao = new UserDao($entityManager);
		
		$response = array('status' => "FAIL", 'message' => "Error occurred try again later");
		
		$dbCheck = $userDao->findByEmailAndPassword($email, md5($password));
		if(!empty($dbCheck)){//check db for is email already registered
			$user = $dbCheck[0];
			$_SESSION['token'] = $email;
			if($user->getRole()=="USER"){
				$_SESSION['role'] = "USER";	
			}else if($user->getRole()=="ADMIN"){
				$_SESSION['role'] = "ADMIN";
			}
			$response = array('status' => "SUCCESS", 'message' => "Logged in successfully");
			echo json_encode($response);
		}else{
			$response = array('status' => "FAIL", 'message' => "Incorrect email or password");
			echo json_encode($response);
		}
	}
 ?>