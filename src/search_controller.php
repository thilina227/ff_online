<?php
require_once 'bootstrap.php';
require_once 'orm/dao/item_dao.php';

use model\Item;
use dao\ItemDao;
$itemDao = new ItemDao($entityManager);

if ($_POST) {
	$search = $_POST['search'];
	
	$response = array('status' => "FAIL", 'message' => "No results found");
	
	$items = $itemDao->findLike($search);
	
	$itemsList = array();
	foreach ($items as $key => $item) {
		$itemArr = array('id'=> $item->getId(),
					 'name' => $item->getName(), 
					 'price' => $item->getPrice(), 
					 'category' => $item->getCategory(), 
					 'subCategory'=> $item->getSubCategory());
		
		array_push($itemsList, $itemArr);
	}
	
	if(!empty($items)){
		$response = array('status' => "SUCCESS", 'items' => $itemsList);	
	}
	
	echo json_encode($response);

}
?>