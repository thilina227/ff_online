<?php
session_start();
//edit_item_controller.php
require_once 'bootstrap.php';
require_once 'orm/dao/item_dao.php';
require_once 'orm/dao/sub_category_dao.php';
require_once 'orm/dao/category_dao.php';

use model\SubCategory;
use dao\SubCategoryDao;
use model\Category;
use dao\CategoryDao;
use model\Item;
use dao\ItemDao;

if ($_POST) {
	$id = $_POST['id'];	
	$name = $_POST['name'];
	$price = $_POST['price'];
	$categoryId = $_POST['category'];
	$subCategoryId = $_POST['subCategory'];

	$itemDao = new ItemDao($entityManager);

	$item = $itemDao->findById($id);
	
	$categoryDao = new CategoryDao($entityManager);
	$subCategoryDao = new SubCategoryDao($entityManager);
	
	//fetch category
	$category = $categoryDao->findById($categoryId);
	//fetch subCategories
	$subCategory = $subCategoryDao->findById($subCategoryId);
	
	 
	//creaet item object
	$item -> setName($name);
	$item -> setPrice($price);
	$item -> setCategory($category);
	$item -> setSubCategory($subCategory);

	$ImageFile = $_FILES['image'];
	if(!empty($ImageFile)){
		$image = "uploaded/".$id.".jpg";
		move_uploaded_file($_FILES["image"]["tmp_name"], $image);
		$item -> setImage($image);
	}

	$response = array('status' => "FAIL", 'field' => '', 'message' => "Error occured try again later");
	$itemDao -> save($item);	
	//save item
	$response = array('status' => "SUCCESS", 'field' => '', 'message' => "Item updated successfully");
	// echo json_encode($response);
	
	$_SESSION['message'] = "Item updated successfully";
	header('Location: edit_item.php?action=edit&id='.$id);

}
?>