<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
		include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section class="row full-width">
			<form action="#" id="signup-form" method="post" accept-charset="utf-8">
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="first_name">First Name</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="text" required="true" placeholder="First Name" name="first_name" value="" id="first_name"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="last_name">Last Name</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="text" required="true" name="last_name" placeholder="Last Name" value="" id="last_name"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="phone">Phone number</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="text" required="true" type="number" name="phone" placeholder="Phone number" value="" id="phone"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="email">Email</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="email" name="email" placeholder="Email" value="" id="email"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="address">Address</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="text" name="address" placeholder="Address" value="" id="address"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="password">Password</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="password" name="password" placeholder="**********" value="" id="password"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="password_confirm">Re-type Password</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="password" name="password_confirm" placeholder="**********" value="" id="password_confirm"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<br />
					</div>
					<div class="large-4 medium-4 columns end">
						<input id="btn-signup" class="button secondary radius" type="submit" value="Sign Up"/>
					</div>
				</div>
			</form>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
		<style type="text/css" media="screen">
			.lbl {
				text-align: right;
				padding-top: 10px;
				color: #FFFFFF;
			}
			#signup-form {
				margin: 20px;
			}

		</style>
		<script type="text/javascript" charset="utf-8">
			var request;
			$(document).ready(function() {
				
				$("#btn-signup").click(function() {
					$(".error").hide();
					if(validate()) {
						submitForm();
					}
					return false;
				});
				
			});
						
			$.fn.serializeObject = function(){//serialize form data
			    var o = {};
			    var a = this.serializeArray();
			    $.each(a, function() {
			        if (o[this.name] !== undefined) {
			            if (!o[this.name].push) {
			                o[this.name] = [o[this.name]];
			            }
			            o[this.name].push(this.value || '');
			        } else {
			            o[this.name] = this.value || '';
			        }
			    });
			    return o;
			};						

			//ajax submit form data
			function submitForm(){
				var values = $("#signup-form").serializeObject();
				 $.ajax({
			        url: "signup_controller.php",
			        type: "post",
			        data: values,
			        success: function(result){
			            var obj = $.parseJSON(result);
			            if(obj['status'] == "FAIL" && obj['field'] == "email"){
			            	$("#email").after('<small class="error">'+obj['message']+'</small>');
			            }else if(obj['status'] == "SUCCESS"){
			            	$("#btn-signup").after('<small style="margin-left:20px;" class="success">'+obj['message']+'</small>');
			            	window.location.replace("login.php");
			            }else{
			            	$("#btn-signup").after('<small class="success">An error occurred, please try again later</small>');
			            }
			            console.log(result)
			        },
			        error:function(result){
			            alert(result);
			            console.log(result)
			        }
			    });
			};			
			
			function validate() {//client side validation user inputs
				var firstName = $("#first_name").val();
				var lastName = $("#last_name").val();
				var phone = $("#phone").val();
				var email = $("#email").val();
				var address = $("#address").val();
				var password = $("#password").val();
				var passwordConfirm = $("#password_confirm").val();

				var formParams = "first name = " + firstName + "\n" + "lastname = " + lastName + "\n" + "phone = " + phone + "\n" + "email= " + email + "\n" + "address = " + address + "\n" + "password =" + password + "\n" + "confirm password = " + passwordConfirm;
				console.log(formParams);
		
				if(firstName == ""){
					$("#first_name").after('<small class="error">Required</small>');
					return false;
				}
				if(lastName == ""){
					$("#last_name").after('<small class="error">Required</small>');
					return false;
				}
				
				var paternPhoneno1 = /^(\+94)([0-9]{9})$/;
				var paternPhoneno2 = /^(94)([0-9]{9})$/;
				var paternPhoneno3 = /^(0)?([0-9]{9})$/;
				if(phone == ""){
					$("#phone").after('<small class="error">Required</small>');
					return false;
				}
				if(phone.match(paternPhoneno1) || phone.match(paternPhoneno2) || phone.match(paternPhoneno3)){
					//matches do nothing
				}else{
					$("#phone").after('<small class="error">Phone number is invalid. (eg: +94123456789/0123456789)</small>');
					return false;
				}
				
				if(email == ""){
					$("#email").after('<small class="error">Required.</small>');
					return false;
				}
				var patternEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
				if(!email.match(patternEmail)){
					$("#email").after('<small class="error">Email address is invalid</small>');
					return false;
				}
				
				if(address == ""){
					$("#address").after('<small class="error">Required.</small>');
					return false;
				}
				
				if(password == ""){
					$("#password").after('<small class="error">Required.</small>');
					return false;
				}
				
				if(passwordConfirm == ""){
					$("#password_confirm").after('<small class="error">Required.</small>');
					return false;
				}
				
				if(password != passwordConfirm){
					$("#password_confirm").after('<small class="error">Password does not match.</small>');
					return false;
				}
				
				return true;
			};
		</script>
	</body>
</html>
