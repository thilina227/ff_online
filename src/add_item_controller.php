<?php
//add_item_controller.php
//controller to handle request from signup.php

session_start();

require_once 'bootstrap.php';
require_once 'orm/dao/item_dao.php';
require_once 'orm/dao/sub_category_dao.php';
require_once 'orm/dao/category_dao.php';

use model\SubCategory;
use dao\SubCategoryDao;
use model\Category;
use dao\CategoryDao;
use model\Item;
use dao\ItemDao;
if ($_POST) {
	$name = $_POST['name'];
	$price = $_POST['price'];
	$categoryId = $_POST['category'];
	$subCategoryId = $_POST['subCategory'];
	$type = $_POST['type'];
	
	$categoryDao = new CategoryDao($entityManager);
	$subCategoryDao = new SubCategoryDao($entityManager);
	//fetch category
	$category = $categoryDao->findById($categoryId);
	//fetch subCategories
	$subCategories = $subCategoryDao->findById($subCategoryId);

	$item = new Item();
	//creaet item object
	$item -> setName($name);
	$item -> setPrice($price);
	$item -> setCategory($category);
	$item -> setSubCategory($subCategories);
	$item -> setType($type);
	$item -> setImage("images/no_image.jpg");

	$itemDao = new ItemDao($entityManager);

	// $response = array('status' => "FAIL", 'field' => '', 'message' => "Error occured try again later");
	$saved_item = $itemDao -> save($item);
	$id = $saved_item->getId();
	
	echo "id=".$id;
	
	$ImageFile = $_FILES['image'];
	print_r($_FILES);
	
	$image = "uploaded/".$id.".jpg";
	move_uploaded_file($_FILES["image"]["tmp_name"], $image);

	$saved_item->setImage($image);
	$itemDao -> save($item);
	
	$_SESSION['message'] = "Item added successfully";
	header('Location: add_item.php');
	
	
}
?>