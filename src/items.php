<?php 
require_once 'bootstrap.php';
require_once 'orm/dao/item_dao.php';
require_once 'orm/dao/sub_category_dao.php';
require_once 'orm/dao/category_dao.php';

use model\SubCategory;
use dao\SubCategoryDao;
use model\Category;
use dao\CategoryDao;
use model\Item;
use dao\ItemDao;
?>
<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		?>
		<!-- section -->
		<section id="section" class="row full-width">
			<div class="large-2 columns">
				<p></p>
			</div>
			<div class="large-8 columns">
				<h2>All Items</h2>
				<table>
					<thead>
						<tr>
							<th width="10">#</th>
							<th style="width: 60%;">Item name</th>
							<th width="200">Type</th>
							<th width="200">Category</th>
							<th width="200">Sub Category</th>
							<th width="200">Price</th>
							<th width="200">Operation</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$itemDao = new ItemDao($entityManager);
							$items = $itemDao->getAll();
							foreach ($items as $key => $item) {
								//print table row
								print "<tr><td>".$item->getId()."</td><td>".$item->getName()."</td><td>".$item->getType()."</td><td>".$item->getCategory()->getName()."</td><td>".$item->getSubCategory()->getName()."</td><td>".$item->getPrice()."</td><td><a class=\"delete\" href=\"item_controller.php?action=delete&id=".$item->getId()."\">delete</a>/<a href=\"edit_item.php?action=edit&id=".$item->getId()."\">Edit</a></td></tr>";
							}
						 ?>
					</tbody>
				</table>
			</div>
			<div class="large-2 columns">
				<p></p>
			</div>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
	</body>
	<script type="text/javascript" charset="utf-8">
		
	</script>
	<style type="text/css" media="screen">
		#section{
			min-height: 390px;
		}
	</style>
</html>
