<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';

		require_once 'bootstrap.php';
		require_once 'orm/dao/item_dao.php';
		require_once 'orm/dao/category_dao.php';
		require_once 'orm/dao/sub_category_dao.php';
		
		use model\Item;
		use model\Category;
		use model\SubCategory;
		use dao\ItemDao;
		use dao\CategoryDao;
		use dao\SubCategoryDao;

		if ($_GET) {
			$id = $_GET['id'];
			$itemDao = new ItemDao($entityManager);
			$item = $itemDao->findById($id);
			if($item == NULL){
				echo "Could not find the item";
			}else{
		
		?>
		<!-- section -->
		<section class="row full-width">
			<form id="item_form" action="edit_item_controller.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<h2>Update Item</h2>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<?php if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
								echo "<h3 class=\"success\">".$_SESSION['message']."</h3>";
								$_SESSION['message'] = "";
							}
						?>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<?php if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
								echo "<h3 class=\"success\">".$_SESSION['message']."</h3>";
								$_SESSION['message'] = "";
							}
						?>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
						<input type="hidden" required="true" name="id" value="<?php echo $item->getId(); ?>" id="name"/>
					</div>
					<div class="large-4 medium-4 columns end">
						Item name
						<input type="text" required="true" name="name" placeholder="Name" value="<?php echo $item->getName(); ?>" id="name"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Type
						<br/>
						<select class="large-4 medium-4 columns end" style="width: 100%;" id="type" name="type">
							<option value="FOOD" <?php if($item->getType() == "FOOD")echo "selected=\"true\""; ?>> Food </option>
							<option value="BEAVERAGE" <?php if($item->getType() == "BEAVERAGE")echo "selected=\"true\""; ?>> Beaverage </option>
						</select>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Category
						<br/>
						<select class="large-4 medium-4 columns end" style="width: 100%;" onchange="loadSubCategories();" id="category" name="category">
							<option value="0">--Select--</option>
							<?php 
								$categoryDao = new CategoryDao($entityManager);
								$categories = $categoryDao->getall();
								foreach ($categories as $key => $cagetory) {
									if($item->getCategory()->getId()==$cagetory->getId()){
										echo "<option value=\"".$cagetory->getId()."\" selected=\"true\">".$cagetory->getName()."</option>";
									}else{
										echo "<option value=\"".$cagetory->getId()."\">".$cagetory->getName()."</option>";
									}
								}
							 ?>
						</select>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Sub Category
						<br/>
						<select class="large-4 medium-4 columns end" style="width: 100%;" id="subCategory" name="subCategory">
							<option id="default_sub_category" value="0">--Select--</option>
							<?php 
								$subCategoryDao = new SubCategoryDao($entityManager);
								foreach ($subCategoryDao->findByCategory($item->getCategory()) as $key => $subCategory) {
									if($item->getSubCategory()->getId()==$subCategory->getId()){
										echo "<option class=\"sub_category\" value=\"".$subCategory->getId()."\" selected=\"true\">".$subCategory->getName()."</option>";
									}else{
										echo "<option class=\"sub_category\" value=\"".$subCategory->getId()."\">".$subCategory->getName()."</option>";
									}
								}
								
							 ?>
						</select>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Price
						<input type="number" required="true" name="price" placeholder="Price" value="<?php echo $item->getPrice(); ?>" id="price"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Image
						<img id="uploadPreview" src="<?php if($item->getImage()==NULL or $item->getImage()=="")echo "images/no_image.jpg";else echo $item->getImage(); ?>" /><br />
						<input required="true" id="image" type="file" name="image" onchange="PreviewImage();" />
					</div>
				</div>
				
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<br />
					</div>
					<div class="large-4 medium-4 columns end">
						<input id="btn_add" class="button secondary radius" type="submit" value="Update"/>
					</div>
				</div>
			</form>
		</section>
		<!-- footer -->
		<?php
			}
		}
		include 'template/footer.php';
		?>
	</body>
	<style type="text/css" media="screen">
		.lbl {
			text-align: right;
			padding-top: 10px;
			color: #FFFFFF;
		}
		#signup-form {
			margin: 20px;
		}

	</style>
	<script type="text/javascript" charset="utf-8">
	
	function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    }
	
		$(document).ready(function() {

			$("#btn_add").click(function() {
				$(".error").hide();
				$(".success").hide();
				if(validate()) {
					submitForm();
				}
				return false;
			});
		});
		//ajax submit form data
		function submitForm() {
			$("#item_form").submit();
			// var values = $("#item_form").serializeObject();
			// $.ajax({
				// url : "edit_item_controller.php",
				// type : "post",
				// data : values,
				// success : function(result) {
					// alert(result);
					// var obj = $.parseJSON(result);
					// if(obj['status'] == "SUCCESS") {
						// $("#btn_add").after('<small style="margin-left:20px;" class="success">' + obj['message'] + '</small>');
						// $("#name").val("");
						// $("#price").val("");
						// window.location.replace('items.php');
					// } else {
						// $("#btn_add").before('<small class="error">An error occurred, please try again later</small>');
					// }
					// console.log(result)
				// },
				// error : function(result) {
					// alert(result);
					// console.log(result)
				// }
			// });
		};


		$.fn.serializeObject = function() {//serialize form data
			var o = {};
			var a = this.serializeArray();
			$.each(a, function() {
				if(o[this.name] !== undefined) {
					if(!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});
			return o;
		};
		function validate() {//client side validation user inputs
			var itemname = $("#name").val();
			var price = $("#price").val();
			var category = $("#category").val();
			var subCategory = $("#subCategory").val();

			if(itemname == "") {
				$("#name").after('<small class="error">Required.</small>');
				return false;
			}
			if(price == "") {
				$("#price").after('<small class="error">Required.</small>');
				return false;
			}
			
			if(category == 0) {
				$("#category").after('<small class="error">Required.</small>');
				return false;
			}
			
			if(subCategory == 0) {
				$("#subCategory").after('<small class="error">Required.</small>');
				return false;
			}
			
			return true;
		};
		
		function loadSubCategories() {
			$(".sub_category").hide();
			$("#default_sub_category").attr('selected', true);
			var category = $("#category").val();
			$.ajax({
				url : "sub_category_controller.php",
				type : "post",
				data : {'category':category},
				success : function(result) {
					var obj = $.parseJSON(result);
					if(obj['status'] == "SUCCESS") {
						obj['subCategories'].forEach(function (subCategory){
							$('#default_sub_category').after('<option class="sub_category" value="'+subCategory['id']+'">'+subCategory['name']+'</option>');
						});
					}
					console.log(result)
				},
				error : function(result) {
					alert(result);
					console.log(result)
				}
			});
		};
	</script>
</html>
