<?php
//adding item to the cart in the session
session_start();

require_once 'bootstrap.php';
require_once 'orm/dao/cart_dao.php';
require_once 'orm/dao/user_dao.php';
require_once 'orm/dao/item_dao.php';
require_once 'orm/dao/cart_item_dao.php';

use model\Item;
use model\Cart;
use model\CartItem;
use model\User;
use dao\UserDao;
use dao\ItemDao;
use dao\CartDao;
use dao\CartItemDao;

	if (isset($_SESSION['token']) && isset($_SESSION['role']) && $_SESSION['role'] == "USER") {
		if ($_POST) {
			$userDao = new UserDao($entityManager);
			$cartDao = new CartDao($entityManager);
			$cartItemDao = new CartItemDao($entityManager);
			$itemDao = new ItemDao($entityManager);

			if (isset($_POST['action']) && $_POST['action'] == "add") {
				$itemId = $_POST['id'];
				$qty = $_POST['qty'];

				$user = $userDao->findByEmail($_SESSION['token']);

				if(!empty($user)){
					$user = $user[0];
					$cart = $cartDao -> findByUser($user);
					if(empty($cart)){
						$cart = new Cart();
						$cart->setUser($user);
						$cartDao->save($cart);
					}else{
						$cart = $cart[0];						
					}
					
					$item = $itemDao->findById($itemId);
					if(!empty($item)){
						$cartItem = new CartItem();
						$cartItem->setCart($cart);
						$cartItem->setItem($item);
						$cartItem->setQty($qty);
						$cartItem->setStatus("PENDING");
						$cartItemDao->save($cartItem);
						echo "{\"status\":\"SUCCESS\",\"message\":\"Added to cart\"}";
					}
				}
	
			} else if (isset($_POST['action']) && $_POST['action'] == "delete") {
				$user = $userDao->findByEmail($_SESSION['token']);
				
				if(!empty($user)){
					$cart = $cartDao -> findByUser($user);
					if(empty($cart)){
						echo "{\"status\":\"FAIL\",\"message\":\"Failed to remove item\"}";
					}
					
					$cartItemId = $_POST['id'];
					$cartItem = $cartItemDao->findById($cartItemId);
					if(empty($cartItem)){
						echo "{\"status\":\"FAIL\",\"message\":\"Failed to remove item\"}";
					}else{
						$cartItemDao->delete($cartItem);
						echo "{\"status\":\"SUCCESS\",\"message\":\"Removed from cart\"}";
					}
				}
			}
		}
	
	} else{
		echo "{\"status\":\"FAIL\",\"message\":\"Authentication failure\"}";
	}
?>
