<?php
//add_item_controller.php
//controller returns sub categories for a requested category
require_once 'bootstrap.php';
require_once 'orm/dao/item_dao.php';
require_once 'orm/dao/sub_category_dao.php';
require_once 'orm/dao/category_dao.php';

use model\SubCategory;
use dao\SubCategoryDao;
use model\Category;
use dao\CategoryDao;

if ($_POST) {
	$categoryId = $_POST['category'];

	$categoryDao = new CategoryDao($entityManager);
	$subCategoryDao = new SubCategoryDao($entityManager);
	//fetch category
	$category = $categoryDao->findById($categoryId);
	//fetch subCategories
	$subCategories = $subCategoryDao->findByCategory($category);

	$subCategoriesArray = array();
	
	foreach ($subCategories as $key => $subCategory) {
		$arr = array("id" => $subCategory->getId(), "name"=>$subCategory->getName());
		array_push($subCategoriesArray, $arr);
	}
	
	$response = array('status' => "SUCCESS", 'subCategories' => $subCategoriesArray);
	echo json_encode($response);

}
?>