<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
	include 'template/head.php';
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';

		require_once 'bootstrap.php';
		require_once 'orm/dao/item_dao.php';
		require_once 'orm/dao/order_dao.php';
		require_once 'orm/dao/cart_dao.php';
		require_once 'orm/dao/cart_item_dao.php';
		
		use model\Item;
		use dao\ItemDao;
		use dao\OrderDao;
		use dao\CartDao;
		use model\CartOrder;
		use dao\CartItemDao;

		if ($_GET) {
			$id = $_GET['id'];
			$orderDao = new OrderDao($entityManager);
			$order = $orderDao->findById($id);
			if($order == NULL){
				echo "Could not find the item";
			}else{
		
		?>
		<!-- section -->
		<section class="row full-width">
			<form id="item_form" action="edit_order_controller.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						<h2>Order: <?php echo $order->getId(); ?></h2>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
						<input type="hidden" required="true" name="id" value="<?php echo $order->getId(); ?>" id="name"/>
					</div>
					<div class="large-4 medium-4 columns end">
						Date &amp; time	
						<input type="text" required="true" name="date" readonly="" placeholder="Name" value="<?php echo $order->getOrderDate()->format('Y-m-d H:i:s'); ?>" id="date"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						User
						<br/>
						<input type="text" required="true" name="user" readonly="" value="<?php echo $order->getUser()->getFirstName()." ".$order->getUser()->getLastName() ?>" id="user"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Contact
						<br/>
						<input type="text" required="true" name="phone" readonly="" value="<?php echo $order->getPhone()?>" id="phone"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Address
						<br/>
						<textarea readonly="" rows="4"><?php echo $order->getAddress()?></textarea>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Total
						<input type="number" readonly="" required="true" name="total" placeholder="total" value="<?php echo $order->getTotal(); ?>" id="total"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Items
						<?php 
							$cartItemDao = new CartItemDao($entityManager);
							$cartItems = $cartItemDao->findByCart($order->getCart());
							if($cartItems != NULL){
						 ?>
						 	<table style="width: 100%;">
								<thead>
									<tr>
										<th>#</th>
										<th>Name</th>
										<th>Qty</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($cartItems as $key => $cartItem) {
											print "<tr><td>".$cartItem->getId()."</td><td>".$cartItem->getItem()->getName()."</td><td>".$cartItem->getQty()."</td></tr>";	
										}
									 ?>
								</tbody>
							</table>
						 <?php 
							} 
							?>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<p></p>
					</div>
					<div class="large-4 medium-4 columns end">
						Status
						<select name="status">
							<option value="PENDING">PENDING</option>
							<option value="DELIVERED">DELIVERED</option>
						</select>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<br />
					</div>
					<input type="hidden" name="id" value="<?php echo $order->getId(); ?>" id="id"/>
					<div class="large-4 medium-4 columns end">
						<input id="btn_add" class="button secondary radius" type="submit" value="Update"/>
					</div>
				</div>
			</form>
		</section>
		<!-- footer -->
		<?php
			}
		}
		include 'template/footer.php';
		?>
	</body>
	<style type="text/css" media="screen">
		.lbl {
			text-align: right;
			padding-top: 10px;
			color: #FFFFFF;
		}
		#signup-form {
			margin: 20px;
		}

	</style>
	<script type="text/javascript" charset="utf-8">
	
	function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    }
	
		$(document).ready(function() {

			$("#btn_add").click(function() {
				$(".error").hide();
				$(".success").hide();
				if(validate()) {
					submitForm();
				}
				return false;
			});
		});
		//ajax submit form data
		function submitForm() {
			$("#item_form").submit();
			// var values = $("#item_form").serializeObject();
			// $.ajax({
				// url : "edit_item_controller.php",
				// type : "post",
				// data : values,
				// success : function(result) {
					// alert(result);
					// var obj = $.parseJSON(result);
					// if(obj['status'] == "SUCCESS") {
						// $("#btn_add").after('<small style="margin-left:20px;" class="success">' + obj['message'] + '</small>');
						// $("#name").val("");
						// $("#price").val("");
						// window.location.replace('items.php');
					// } else {
						// $("#btn_add").before('<small class="error">An error occurred, please try again later</small>');
					// }
					// console.log(result)
				// },
				// error : function(result) {
					// alert(result);
					// console.log(result)
				// }
			// });
		};


		$.fn.serializeObject = function() {//serialize form data
			var o = {};
			var a = this.serializeArray();
			$.each(a, function() {
				if(o[this.name] !== undefined) {
					if(!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});
			return o;
		};
		function validate() {//client side validation user inputs
			var itemname = $("#name").val();
			var price = $("#price").val();
			var category = $("#category").val();
			var subCategory = $("#subCategory").val();

			if(itemname == "") {
				$("#name").after('<small class="error">Required.</small>');
				return false;
			}
			if(price == "") {
				$("#price").after('<small class="error">Required.</small>');
				return false;
			}
			
			if(category == 0) {
				$("#category").after('<small class="error">Required.</small>');
				return false;
			}
			
			if(subCategory == 0) {
				$("#subCategory").after('<small class="error">Required.</small>');
				return false;
			}
			
			return true;
		};
		
		function loadSubCategories() {
			$(".sub_category").hide();
			$("#default_sub_category").attr('selected', true);
			var category = $("#category").val();
			$.ajax({
				url : "sub_category_controller.php",
				type : "post",
				data : {'category':category},
				success : function(result) {
					var obj = $.parseJSON(result);
					if(obj['status'] == "SUCCESS") {
						obj['subCategories'].forEach(function (subCategory){
							$('#default_sub_category').after('<option class="sub_category" value="'+subCategory['id']+'">'+subCategory['name']+'</option>');
						});
					}
					console.log(result)
				},
				error : function(result) {
					alert(result);
					console.log(result)
				}
			});
		};
	</script>
</html>
