<!doctype html>
<html class="no-js" lang="en">
	<!-- head -->
	<?php
		include 'template/head.php';
		require_once 'bootstrap.php';
		require_once 'orm/dao/user_dao.php';
		
		use model\User;
		use dao\UserDao;
	?>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		
		$userDao = new UserDao($entityManager);
		$user = NULL;
		if(isset($_SESSION['token'])){
			$user = $userDao->findByEmail($_SESSION['token']);
			 
			if(!empty($user)){
				$user = $user[0];
			}
		}
		?>
		<!-- section -->
		<section class="row full-width">
			<form action="#" id="signup-form" method="post" accept-charset="utf-8">
				<input type="hidden" required="true" name="id" value="<?php echo $user->getId(); ?>" id=""/>
				<input type="hidden" required="true" name="role" value="<?php echo $user->getRole(); ?>" id=""/>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="first_name">First Name</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="text" required="true" placeholder="First Name" name="first_name" value="<?php echo $user->getFirstName(); ?>" id="first_name"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="last_name">Last Name</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="text" required="true" name="last_name" placeholder="Last Name" value="<?php echo $user->getLastName(); ?>" id="last_name"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="phone">Phone number</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="text" required="true" type="number" name="phone" placeholder="Phone number" value="<?php echo $user->getPhone(); ?>" id="phone"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="email">Email</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="email" name="email" placeholder="Email" value="<?php echo $user->getEmail(); ?>" id="email"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<label class="lbl" for="address">Address</label>
					</div>
					<div class="large-4 medium-4 columns end">
						<input type="text" name="address" placeholder="Address" value="<?php echo $user->getAddress(); ?>" id="address"/>
					</div>
				</div>
				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<br />
					</div>
					<div class="large-4 medium-4 columns end">
						<a href="#" id="btn_toggle" onclick="togglePwd()" class="button primary">Change Password</a>
					</div>
				</div>
				<div id="pwd" style="display: none;">
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="password">Current Password</label>
						</div>
						<div class="large-4 medium-4 columns end">
							<input type="password" name="cpassword" placeholder="**********" value="" id="cpassword"/>
						</div>
					</div>
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="password">New Password</label>
						</div>
						<div class="large-4 medium-4 columns end">
							<input type="password" name="password" placeholder="**********" value="" id="password"/>
						</div>
					</div>
					<div class="row full-width">
						<div class="large-4 medium-4 columns">
							<label class="lbl" for="password_confirm">Re-type New Password</label>
						</div>
						<div class="large-4 medium-4 columns end">
							<input type="password" name="password_confirm" placeholder="**********" value="" id="password_confirm"/>
						</div>
					</div>
				</div>

				<div class="row full-width">
					<div class="large-4 medium-4 columns">
						<br />
					</div>
					<div class="large-4 medium-4 columns end">
						<input id="btn-signup" class="button secondary radius" type="submit" value="Save"/>
					</div>
				</div>
			</form>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
		<style type="text/css" media="screen">
			.lbl {
				text-align: right;
				padding-top: 10px;
				color: #FFFFFF;
			}
			#signup-form {
				margin: 20px;
			}

		</style>
		<script type="text/javascript" charset="utf-8">
			var request;
			$(document).ready(function() {
				
				$("#btn-signup").click(function() {
					$(".error").hide();
					if(validate()) {
						submitForm();
					}
					return false;
				});
				
			});
						
			$.fn.serializeObject = function(){//serialize form data
			    var o = {};
			    var a = this.serializeArray();
			    $.each(a, function() {
			        if (o[this.name] !== undefined) {
			            if (!o[this.name].push) {
			                o[this.name] = [o[this.name]];
			            }
			            o[this.name].push(this.value || '');
			        } else {
			            o[this.name] = this.value || '';
			        }
			    });
			    return o;
			};						

			//ajax submit form data
			function submitForm(){
				var values = $("#signup-form").serializeObject();
				 $.ajax({
			        url: "myaccount_controller.php",
			        type: "post",
			        data: values,
			        success: function(result){
			            var obj = $.parseJSON(result);
			            if(obj['status'] == "FAIL" && obj['field'] == "email"){
			            	$("#email").after('<small class="error">'+obj['message']+'</small>');
			            } if(obj['status'] == "FAIL" && obj['field'] == "cpassword"){
			            	$("#cpassword").after('<small class="error">'+obj['message']+'</small>');
			            }else if(obj['status'] == "SUCCESS"){
			            	$("#btn-signup").after('<small style="margin-left:20px;" class="success">'+obj['message']+'</small>');
			            }else{
			            	$("#btn-signup").after('<small class="error">An error occurred, please try again later</small>');
			            }
			            console.log(result)
			        },
			        error:function(result){
			            alert(result);
			            console.log(result)
			        }
			    });
			};			
			
			function validate() {//client side validation user inputs
				var firstName = $("#first_name").val();
				var lastName = $("#last_name").val();
				var phone = $("#phone").val();
				var email = $("#email").val();
				var address = $("#address").val();
				var password = $("#password").val();
				var cpassword = $("#cpassword").val();
				var passwordConfirm = $("#password_confirm").val();

				var formParams = "first name = " + firstName + "\n" + "lastname = " + lastName + "\n" + "phone = " + phone + "\n" + "email= " + email + "\n" + "address = " + address + "\n" + "password =" + password + "\n" + "current password = " + cpassword+ "\nconfirm password = " + passwordConfirm;
				console.log(formParams);
		
				if(firstName == ""){
					$("#first_name").after('<small class="error">Required</small>');
					return false;
				}
				if(lastName == ""){
					$("#last_name").after('<small class="error">Required</small>');
					return false;
				}
				
				var paternPhoneno1 = /^(\+94)([0-9]{9})$/;
				var paternPhoneno2 = /^(94)([0-9]{9})$/;
				var paternPhoneno3 = /^(0)?([0-9]{9})$/;
				if(phone == ""){
					$("#phone").after('<small class="error">Required</small>');
					return false;
				}
				if(phone.match(paternPhoneno1) || phone.match(paternPhoneno2) || phone.match(paternPhoneno3)){
					//matches do nothing
				}else{
					$("#phone").after('<small class="error">Phone number is invalid. (eg: +94123456789/0123456789)</small>');
					return false;
				}
				
				if(email == ""){
					$("#email").after('<small class="error">Required.</small>');
					return false;
				}
				var patternEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
				if(!email.match(patternEmail)){
					$("#email").after('<small class="error">Email address is invalid</small>');
					return false;
				}
				
				if(address == ""){
					$("#address").after('<small class="error">Required.</small>');
					return false;
				}
				
				if($("#pwd").is(":visible")){
					if(password == ""){
						$("#password").after('<small class="error">Required.</small>');
						return false;
					}
					
					if(passwordConfirm == ""){
						$("#password_confirm").after('<small class="error">Required.</small>');
						return false;
					}
					
					if(password != passwordConfirm){
						$("#password_confirm").after('<small class="error">Password does not match.</small>');
						return false;
					}
				}
				
				return true;
			};
			
			function togglePwd(){
				if($("#pwd").is(":visible")){
					$("#pwd").slideUp(200);
				}else{
					$("#pwd").slideDown(200);
				}
			}
		</script>
	</body>
</html>
