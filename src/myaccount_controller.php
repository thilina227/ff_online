<?php
//signup_controller.php
//controller to handle request from signup.php 
require_once 'bootstrap.php';
require_once 'orm/dao/user_dao.php';

use model\User;
use dao\UserDao;
	if($_POST){
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$address = $_POST['address'];
		$password = $_POST['password'];
		$cpassword = $_POST['cpassword'];
		$role = $_POST['role'];
		$cpassword =  md5($cpassword);
		$id = $_POST['id'];
		
		$userDao = new UserDao($entityManager);
		
		$user = $userDao->findById($id);
		$user->setFirstName($first_name);
		$user->setLastName($last_name);
		$user->setPhone($phone);
		$user->setEmail($email);
		$user->setRole($role);
		$user->setAddress($address);
		
		$error = false;
		
		if(!empty($password)){
			if($user->getPassword() != $cpassword){
				$response = array('status' => "FAIL", 'field' => 'cpassword', 'message' => "Current password is incorrect");
				echo json_encode($response);;
				$error = true;
			}else{
				$user->setPassword($password);	
			}
		}
		
		if($error == false){
			$response = array('status' => "FAIL", 'field' => '', 'message' => "");
		
			$dbCheck = $userDao->findByEmail($email);
			if(empty($dbCheck) || $dbCheck[0]->getId() == $user->getId()){//check db for is email already registered
				$userDao->save($user);//save user
				$response = array('status' => "SUCCESS", 'field' => '', 'message' => "Saved successfully");
				echo json_encode($response);
			}else{
				$response = array('status' => "FAIL", 'field' => "email", 'message' => "Email address is already in use");
				echo json_encode($response);
			}
				
		}
	}
 ?>