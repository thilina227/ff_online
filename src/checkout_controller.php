<?php
	session_start();
	
	require_once 'bootstrap.php';
	require_once 'orm/dao/cart_dao.php';
	require_once 'orm/dao/user_dao.php';
	require_once 'orm/dao/item_dao.php';
	require_once 'orm/dao/order_dao.php';
	require_once 'orm/dao/cart_item_dao.php';
	
	use model\CartItem;
	use dao\UserDao;
	use dao\ItemDao;
	use dao\CartDao;
	use dao\CartItemDao;
	use dao\OrderDao;
	use model\CartOrder;
	
	if ($_POST) {
		$address = $_POST['address'];
		$phone = $_POST['phone'];
		$dateTime = $_POST['date'];
		$date = NULL;
		if(!empty($dateTime)){
			$date =  new \DateTime($dateTime);
		}
	
		$cartDao = new CartDao($entityManager);
		$cartItemDao = new CartItemDao($entityManager);
		$userDao = new UserDao($entityManager);
		$orderDao = new OrderDao($entityManager);
		$total = 0;
	
		if (isset($_SESSION['token'])) {
			$user = $userDao -> findByEmail($_SESSION['token']);
			if (!empty($user)) {
				$user = $user[0];
				$cart = $cartDao -> findByUser($user);
				if (!empty($cart)) {
					$cart = $cart[0];
					$cartItems = $cartItemDao -> findByCart($cart);
					if (!empty($cartItems)) {
						$i = 1;
						foreach ($cartItems as $key => $cartItem) {
							$total = $total + ($cartItem -> getItem() -> getPrice() * $cartItem -> getQty());
							$cartItem->setStatus("ORDERED");
						}
						
						$order = new CartOrder();
						$order->setCart($cart);
						$order->setUser($user);
						$order->setTotal($total);
						$order->setStatus("PENDING");
						$order->setPhone($phone);
						$order->setAddress($address);
						$order->setOrderDate($date);
						
						$orderDao->save($order);
						
						$_SESSION['message'] = "Order placed successfully";
						header( 'Location: checkout.php' ) ;
					}
				}
			}
		}
	}
?>
