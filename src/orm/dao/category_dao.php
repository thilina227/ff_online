<?php
namespace dao;

require_once (dirname(__FILE__) . '/../../bootstrap.php');
require_once (dirname(__FILE__) . '/../model/category.php');
require_once (dirname(__FILE__) . '/dao.php');

use model\Category;
use Doctrine\ORM\EntityManager;

class CategoryDao extends Dao{

 	public function __construct($entityManager) {
    	parent::__construct($entityManager);
    }

	public function findById($id) {
		return parent::findById('model\Category', $id);
	}

	public function getAll(){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('c')
	        ->from('model\Category', 'c');
	   	return $qb->getQuery()->getResult();
	}
	
	
}
?>