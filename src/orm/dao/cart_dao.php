<?php
namespace dao;

require_once (dirname(__FILE__) . '/../../bootstrap.php');
require_once (dirname(__FILE__) . '/../model/item.php');
require_once (dirname(__FILE__) . '/../model/user.php');
require_once (dirname(__FILE__) . '/../model/cart.php');
require_once (dirname(__FILE__) . '/../model/cart_item.php');
require_once (dirname(__FILE__) . '/dao.php');

use model\Category;
use Doctrine\ORM\EntityManager;

class CartDao extends Dao{

 	public function __construct($entityManager) {
    	parent::__construct($entityManager);
    }

	public function findById($id) {
		return parent::findById('model\Cart', $id);
	}

	public function getAll(){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('c')
	        ->from('model\Cart', 'c');
	   	return $qb->getQuery()->getResult();
	}

	public function findByUser($user){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('c')
	        ->from('model\Cart', 'c')
	        ->where('c.user = :user')
	        ->setParameter('user', $user);
	    return $qb->getQuery()->getResult();
	}
}
?>