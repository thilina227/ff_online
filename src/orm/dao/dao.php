<?php 
namespace dao;

require_once (dirname(__FILE__) . '/../../bootstrap.php');

use Doctrine\ORM\EntityManager;

abstract class Dao{
	
	protected $entityManager;
	
	public function __construct($entityManager){
		$this->entityManager = $entityManager;
	}
	
	public function save($entity){
		$this->entityManager -> persist($entity);
		$this->entityManager -> flush();
		return $entity;
	}
	
	public function delete($entity) {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
	
	protected function findById($entity, $id) {
        return $this->entityManager->find($entity, $id);
    }
	
	public function getEntityManager(){
		return $this->entityManager;
	}
}

 ?>