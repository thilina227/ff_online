<?php
namespace dao;

require_once (dirname(__FILE__) . '/../../bootstrap.php');
require_once (dirname(__FILE__) . '/../model/order.php');
require_once (dirname(__FILE__) . '/dao.php');

use model\CartOrder;
use Doctrine\ORM\EntityManager;

class OrderDao extends Dao{

 	public function __construct($entityManager) {
    	parent::__construct($entityManager);
    }

	public function findById($id) {
		return parent::findById('model\CartOrder', $id);
	}

	public function getAll(){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('i')
	        ->from('model\CartOrder', 'i');
	   	return $qb->getQuery()->getResult();
	}
	
	public function getAllPending(){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('i')
	        ->from('model\CartOrder', 'i')
			->where('i.status = :status')
	        ->setParameter('status', "PENDING");
	   	return $qb->getQuery()->getResult();
	}
	
}
?>