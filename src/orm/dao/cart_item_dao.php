<?php
namespace dao;

require_once (dirname(__FILE__) . '/../../bootstrap.php');
require_once (dirname(__FILE__) . '/../model/item.php');
require_once (dirname(__FILE__) . '/../model/cart.php');
require_once (dirname(__FILE__) . '/../model/cart_item.php');
require_once (dirname(__FILE__) . '/dao.php');

use model\Category;
use Doctrine\ORM\EntityManager;

class CartItemDao extends Dao{

 	public function __construct($entityManager) {
    	parent::__construct($entityManager);
    }

	public function findById($id) {
		return parent::findById('model\CartItem', $id);
	}

	public function getAll(){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('c')
	        ->from('model\CartItem', 'c');
	   	return $qb->getQuery()->getResult();
	}

	public function findByCart($cart){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('c')
	        ->from('model\CartItem', 'c')
	        ->where('c.cart = :cart')
			->andWhere('c.status = :status')
	        ->setParameter('cart', $cart)
			->setParameter('status', "PENDING");
	    return $qb->getQuery()->getResult();
	}
	
	
}
?>