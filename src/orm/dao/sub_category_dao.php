<?php
namespace dao;

require_once (dirname(__FILE__) . '/../../bootstrap.php');
require_once (dirname(__FILE__) . '/../model/sub_category.php');
require_once (dirname(__FILE__) . '/dao.php');

use model\SubCategory;
use Doctrine\ORM\EntityManager;

class SubCategoryDao extends Dao{

 	public function __construct($entityManager) {
    	parent::__construct($entityManager);
    }

	public function findById($id) {
		return parent::findById('model\SubCategory', $id);
	}

	public function getAll(){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('sc')
	        ->from('model\SubCategory', 'sc');
	   	return $qb->getQuery()->getResult();
	}
	
	public function findByCategory($category){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('sc')
	        ->from('model\SubCategory', 'sc')
	        ->where('sc.category = :category')
	        ->setParameter('category', $category);
	   	return $qb->getQuery()->getResult();
	}
	
}
?>