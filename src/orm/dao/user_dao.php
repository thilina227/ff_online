<?php
namespace dao;

require_once (dirname(__FILE__) . '/../../bootstrap.php');
require_once (dirname(__FILE__) . '/../model/user.php');
require_once (dirname(__FILE__) . '/dao.php');

use model\User;
use Doctrine\ORM\EntityManager;

class UserDao extends Dao{

 	public function __construct($entityManager) {
    	parent::__construct($entityManager);
    }

	public function findById($id) {
		return parent::findById('model\User', $id);
	}

	public function findByEmail($email){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('u')
	        ->from('model\User', 'u')
	        ->where('u.email = :email')
	        ->setParameter('email', $email);
	    return $qb->getQuery()->getResult();
	}
	
	public function findByEmailAndPassword($email, $password){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('u')
	        ->from('model\User', 'u')
	        ->where('u.email = :email and u.password = :password')
	        ->setParameter('email', $email)
			->setParameter('password', $password);
	   	return $qb->getQuery()->getResult();
	}
}
?>