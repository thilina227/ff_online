<?php
namespace dao;

require_once (dirname(__FILE__) . '/../../bootstrap.php');
require_once (dirname(__FILE__) . '/../model/item.php');
require_once (dirname(__FILE__) . '/dao.php');

use model\Item;
use Doctrine\ORM\EntityManager;

class ItemDao extends Dao{

 	public function __construct($entityManager) {
    	parent::__construct($entityManager);
    }

	public function findById($id) {
		return parent::findById('model\Item', $id);
	}

	public function getAll(){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('i')
	        ->from('model\Item', 'i');
	   	return $qb->getQuery()->getResult();
	}
	
	public function findLike($search){
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('i')
	        ->from('model\Item', 'i')
	        ->where('i.name like :search')
	        ->setParameter('search', '%'.$search.'%');
	   	return $qb->getQuery()->getResult();
	}
	
	public function find($type, $category, $subCategory, $search, $offset){
		
		$qb = parent::getEntityManager()->createQueryBuilder();
	    $qb->select('i')
	        ->from('model\Item', 'i');
			
			if(!empty($search)){
				$qb->where('i.name like :search')
				->setParameter('search', '%'.$search.'%');
			}
			
			if(!empty($type)){
				$qb->where('i.type = :type')
				->setParameter('type', $type);
			}
			
			if(!empty($category)){
				$qb->where('i.category = :category')
				->setParameter('category', $category);
			}
			
			if(!empty($subCategory)){
				$qb->where('i.subCategory = :subCategory')
				->setParameter('subCategory', $subCategory);
			}
			
			if(!empty($offset)){
				$offset = 1;
			}
			
			$qb->setFirstResult($offset);
			$qb->setMaxResults(9);
			
	   	return $qb->getQuery()->getResult();
	}
}
?>