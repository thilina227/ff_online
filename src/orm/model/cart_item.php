<?php
//model/cart_item.php
namespace model;

require_once ('item.php');

use model\Item;
/**
 * @Entity
 * @Table(name="cart_item")
 **/
class CartItem {

	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 * **/
	private $id;
	/**
	 * @ManyToOne(targetEntity="Cart", cascade={"detach"}, fetch="EAGER")
	 * */
	private $cart;
	/**
	 * @ManyToOne(targetEntity="Item", cascade={"detach"}, fetch="EAGER")
	 * */
	private $item;
	/**
	 * @Column(type="integer")
	 * */
	private $qty;
	/**
	 * @Column(type="string")
	 * */
	//PENDING => added to cart
	//ORDERED => cart checkout out
	//DELIVERED => delivered
	private $status;
	
	public function setId($id){
		$this->id = $id;
	}
	
	public function getId(){
		return $this->id;
	}
	
	public function getCart(){
		return $this->cart;
	}
	
	public function setCart($cart){
		$this->cart = $cart;
	}
	
	public function getItem(){
		return $this->item;
	}
	
	public function setItem($item){
		$this->item = $item;
	}
	
	public function getQty(){
		return $this->qty;
	}
	
	public function setQty($qty){
		$this->qty = $qty;
	}
	
	public function getStatus(){
		return $this->status;
	}
	
	public function setStatus($status){
		$this->status = $status;
	}
}
?>