<?php
//model/sub_category.php
namespace model;

/**
 * @Entity
 * @Table(name="sub_category")
 **/
class SubCategory {

	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 * **/
	private $id;
	/**
	 * @Column(type="string")
	 * **/
	private $name;
	/**
	 * @ManyToOne(targetEntity="Category", cascade={"all"}, fetch="EAGER")
	 * */
	private $category;
	
	public function setId($id){
		$this->id = $id;
	}
	
	public function getId(){
		return $this->id;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function setname($name){
		$this->name = $name;
	}
	
	public function getCategory(){
		return $this->category;
	}
	
	public function setCategory($category){
		$this->category = $category;
	}
}
?>