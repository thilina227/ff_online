<?php
//model/order.php
namespace model;

require_once ('user.php');
require_once ('cart.php');

use model\User;
use model\Cart;

/**
 * @Entity
 * @Table(name="cart_order")
 **/
class CartOrder {

	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 * **/
	private $id;
	/**
	 * @Column(type="datetime", nullable=true)
	 * **/
	private $orderDate;
	/**
	 * @ManyToOne(targetEntity="User", cascade={"detach"}, fetch="EAGER")
	 * */
	private $user;
	/**
	 * @Column(type="string")
	 * **/
	private $status;
    /**
     * @ManyToOne(targetEntity="Cart", cascade={"detach"}, fetch="EAGER")
     */
	private $cart;
	/**
	 * @Column(type="decimal")
	 * **/
	private $total;
	/**
	 * @Column(type="string")
	 * **/
	private $phone;
	/**
	 * @Column(type="string")
	 * **/
	private $address;
	
	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}
	
	public function getOrderDate(){
		return $this->orderDate;
	}
	
	public function setOrderDate($orderDate){
		$this->orderDate = $orderDate;
	}
	
	public function getUser(){
		return $this->user;
	}
	
	public function setUser($user){
		$this->user = $user;
	}
	
	public function getStatus(){
		return $this->status;
	}
	
	public function setStatus($status){
		$this->status = $status;
	}
	
	public function getCart(){
		return $this->cart;
	}
	
	public function setCart($cart){
		$this->cart = $cart;
	}
	
	public function getTotal(){
		return $this->total;
	}
	
	public function setTotal($total){
		$this->total = $total;
	}
	
	public function getPhone(){
		return $this->phone;
	}
	
	public function setPhone($phone){
		$this->phone = $phone;
	}
	
	public function getAddress(){
		return $this->address;
	}
	
	public function setAddress($address){
		$this->address = $address;
	}
	
}

?>