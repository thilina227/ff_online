<?php
//model/user.php
namespace model;

/**
 * @Entity
 * @Table(name="user")
 **/
class User {

	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 * **/
	private $id;
	/**
	 * @Column(type="string")
	 * **/
	private $firstName;
	/**
	 * @Column(type="string")
	 * **/
	private $lastName;
	/**
	 * @Column(type="string")
	 * **/
	private $email;
	/**
	 * @Column(name="social_id", type="string")
	 * */
	private $phone;
	/**
	 * @Column(type="string")
	 * */
	private $password;
	/**
	 * @Column(type="string")
	 * */
	private $role;
	/**
	 * @Column(type="string")
	 * */
	private $address;

	public function getId() {
		return $this -> id;
	}

	public function getEmail() {
		return $this -> email;
	}

	public function setEmail($email) {
		$this -> email = $email;
	}

	public function getPhone() {
		return $this -> phone;
	}

	public function setPhone($phone) {
		$this -> phone = $phone;
	}

	public function getPassword() {
		return $this -> password;
	}

	public function setPassword($password) {
		$this -> password = md5($password);//save as the hash
	}

	public function getRole() {
		return $this -> role;
	}

	public function setRole($role) {
		$this -> role = $role;
	}

	public function getAddress() {
		return $this -> address;
	}

	public function setAddress($address) {
		$this -> address = $address;
	}

	public function getFirstName(){
		return $this->firstName; 
	}
	
	public function setFirstName($firstName){
		$this->firstName = $firstName;
	}

	public function getLastName(){
		return $this->lastName;
	}
	
	public function setLastName($lastName){
		$this->lastName = $lastName;
	}
	
	public function __toString() {
		return "User: {id: {$this->id}, email: {$this->email}, phone: {$this->phone}}";
	}

}
?>