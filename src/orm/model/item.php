<?php
//model/item.php
namespace model;

require_once 'category.php';
require_once 'sub_category.php';

/**
 * @Entity
 * @Table(name="item")
 **/
class Item {

	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 * **/
	private $id;
	/**
	 * @Column(type="string")
	 * **/
	private $name;
	/**
	 * @Column(type="string")
	 * **/
	private $type;
	/**
	 * @Column(type="decimal")
	 * **/
	private $price;
	/**
	 * @ManyToOne(targetEntity="Category", cascade={"detach"}, fetch="EAGER")
	 * */
	private $category;
	/**
	 * @ManyToOne(targetEntity="SubCategory", cascade={"detach"}, fetch="EAGER")
	 * */
	private $subCategory;
	/**
	 * @Column(type="string")
	 * **/
	private $image;
	// /**
	 // * @Column(type="string")
	 // * **/
	// private $options;
// 	
	
	public function getId() {
		return $this -> id;
	}

	public function setId($id){
		$this->id = $id;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function setname($name){
		$this->name = $name;
	}
	
	public function getPrice(){
		return $this->price;
	}
	
	public function setPrice($price){
		$this->price = $price;
	}
	
	public function getCategory(){
		return $this->category;
	}
	
	public function setCategory($category){
		$this->category = $category;
	}
	
	public function getSubCategory(){
		return $this->subCategory;
	}
	
	public function setSubCategory($subCategory){
		$this->subCategory = $subCategory;
	}
	
	public function getImage(){
		return $this->image;
	}
	
	public function setImage($image){
		$this->image = $image;
	}
	
	public function getType(){
		return $this->type;
	}
	
	public function setType($type){
		$this->type = $type;
	}
}
?>