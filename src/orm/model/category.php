<?php
//model/category.php
namespace model;

/**
 * @Entity
 * @Table(name="category")
 **/
class Category {

	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 * **/
	private $id;
	/**
	 * @Column(type="string")
	 * **/
	private $name;
	
	public function setId($id){
		$this->id = $id;
	}
	
	public function getId(){
		return $this->id;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function setname($name){
		$this->name = $name;
	}
}
?>