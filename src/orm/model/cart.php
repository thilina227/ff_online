<?php
//model/cart.php
namespace model;

/**
 * @Entity
 * @Table(name="cart")
 **/
class Cart {

	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 * **/
	private $id;
	/**
	 * @ManyToOne(targetEntity="User", cascade={"all"}, fetch="EAGER")
	 * */
	private $user;
	
	public function setId($id){
		$this->id = $id;
	}
	
	public function getId(){
		return $this->id;
	}
	
	public function getUser(){
		return $this->user;
	}
	
	public function setUser($user){
		$this->user = $user;
	}
}
?>