<?php 
// bootstrap.php
// Include Composer Autoload (relative to project root).
require_once(dirname(__FILE__)."/vendor/autoload.php");

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// the connection configuration
// TODO load configurations from external file
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => 'root',
    'dbname'   => 'ff_online',
);

$isDevMode = true;$config = Setup::createAnnotationMetadataConfiguration(array("orm/model/"), $isDevMode);

global $entityManager;
$entityManager = EntityManager::create($dbParams, $config);

?>