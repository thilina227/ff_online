<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>FF Online</title>
		<link rel="stylesheet" href="css/foundation.css" />
		<link rel="stylesheet" href="css/ffo.css" />
		<script src="js/vendor/modernizr.js"></script>
	</head>
	<body>
		<!-- header -->
		<?php
		include 'template/header.php';
		?>
		<!-- nav -->
		<?php
		include 'template/nav.php';
		
		require_once 'bootstrap.php';
		require_once 'orm/dao/item_dao.php';
		require_once 'orm/dao/category_dao.php';
		require_once 'orm/dao/cart_dao.php';
		require_once 'orm/dao/user_dao.php';
		require_once 'orm/dao/cart_item_dao.php';
		require_once 'orm/dao/sub_category_dao.php';
		
		use model\Item;
		use dao\UserDao;
		use dao\CartDao;
		use dao\CartItemDao;
		use model\Category;
		use model\SubCategory;
		use dao\ItemDao;
		use dao\CategoryDao;
		use dao\SubCategoryDao;
		
		?>
		<!-- section -->
		<section>
			<div class="row" >
				<div class="large-12 columns" style="text-align: center; z-index: 99999;">
					<form action="search.php" method="get" id="search_form" accept-charset="utf-8">
						<div class="row collapse postfix-round">
							<div class="small-3 columns">
								<label style="text-align: center; padding-left: 10px; vertical-align: middle; display: table-cell; margin: 0px; font-size: large; height: 50px;" for="search_box">What would you like:</label>
							</div>
							<div class="small-7 columns">
								<input name="search" id="search_box" type="text" placeholder="Search..." value="<?php if(isset($_GET['search']))echo $_GET['search'];?>" style="margin: 0px; height: 50px;"/>
							</div>
							<div class="small-2 columns">
								<div id="btn_search" class="postfix">
									<img src="images/search.png" alt="Search" style="height: 50px; padding: 5px;"/>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			
			</div>
			<div id="results" class="row full-width full-height">
				
				<div id="side" class="large-2 columns">
					<ul class="list">
						
						<?php
							$categoryDao = new CategoryDao($entityManager);
							$subCategoryDao = new SubCategoryDao($entityManager);
							$categories = $categoryDao -> getall();
							foreach ($categories as $key => $cagetory) {
								echo "<li>";
								echo "<span>".$cagetory->getName()."</span>";
								foreach ($subCategoryDao->findByCategory($cagetory) as $key => $subCategory) {
									
									echo "<ul>";
									echo "<li>";
									echo "<a href=\"search.php?subCategory=".$subCategory->getId()."\">";
									if(isset($_GET['subCategory']) && $_GET['subCategory'] == $subCategory->getId())
										echo $subCategory->getName()."&nbsp;&nbsp;&nbsp;>>";
									else
										echo $subCategory->getName();
									echo "</a>";
									echo "</li>";
									echo "</ul>";
								}
								echo "</li>";
							}
						 ?>
					</ul>
				</div>
				
				<div id="section" style="text-align: center;" class="large-8 columns full-height">
				
					<div id="topic" style="text-align: left; padding-left: 50px;" class="row">
						  	<?php 
						  	if(isset($_GET['subCategory'])){
								$subCategory = $subCategoryDao->findById($_GET['subCategory']);
								if(!empty($subCategory)){
									echo "<h2>".$subCategory->getName().":</h2>";
								}
						  	}					
							 ?>
					</div>
					<?php 
					
					if($_GET){
						
						$subCategory = "";
						$category = "";
						$type = "";
						$search = "";
						$page = 1;
						
						if(isset($_GET['subCategory'])){
							$subCategory = $_GET['subCategory'];
						}
						if(isset($_GET['category'])){
							$category = $_GET['category'];
						}
						if(isset($_GET['type'])){
							$type = $_GET['type'];
						}
						if(isset($_GET['search'])){
							$search = $_GET['search'];
						}
						if(isset($_GET['page'])){
							$page = $_GET['page'];
						}
			
						$itemDao = new ItemDao($entityManager);
						
						$items = $itemDao->find($type, $category, $subCategory, $search, $page);
						
						if(empty($items)){
							echo "<h2>No results found</h2>";
						}
						
						foreach ($items as $key => $item) {
						?>
						
						<div class="item">
							<div class="details"><?php echo $item->getName(); ?></div>
							<img src="<?php echo $item->getImage(); ?>"/>
							<div class="details" >Rs. <?php echo $item->getPrice(); ?></div>
							<div class="details">
								<div class="inline" style="width:80%; height: 37px;">Number of portions:</div>
								<input class="inline"  style="width:20%; float:right;" type="number" value="1"  id="qty_<?php echo $item->getId(); ?>"/>
							</div>
							<!-- <div class="details">
								 <input type="radio" name="option" value="Red" id="option_<?php echo $item->getId(); ?>"><label for="pokemonRed">Red</label>
							</div> -->
							<div class="btn_add details" onclick="addToCart(<?php echo $item->getId(); ?>)">
								Add to cart<img style="width: 30px; height:30px;" src="images/cart.png"/>
							</div>
						</div>
					<?php
						}
					}
					
					
					 ?>
				</div>
				<div class="large-2 columns">
					<div id="cart_summary">
					  	Your total cost:
					  	<?php 
					  	
					  		$cartDao = new CartDao($entityManager);
							$cartItemDao = new CartItemDao($entityManager);
							$userDao = new UserDao($entityManager);		
							$total = 0;							
							if(isset($_SESSION['token'])){
								$user = $userDao->findByEmail($_SESSION['token']);
								if(!empty($user)){
									$user = $user[0];
									$cart = $cartDao->findByUser($user);
									if(!empty($cart)){
										$cart = $cart[0];
										$cartItems = $cartItemDao->findByCart($cart);
										if(!empty($cartItems)){
											foreach ($cartItems as $key => $cartItem) {
												$total = $total + ($cartItem->getItem()->getPrice()*$cartItem->getQty());
											}
											echo $total;
										}
									}
								}
							}
							
							//show more button only if total != 0
					  		if($total != 0){
					  	 ?>
					  	 <br/>
					  	 <br/>
					  	 <a href="mycart.php" id="btn_mycart">More>></a>
					  	 
					  	 <?php 
							} 
							?>
					  	 
					</div>
				</div>
			</div>
		</section>
		<!-- footer -->
		<?php
		include 'template/footer.php';
		?>
		<script src="js/jquery.expandable.list.js" type="text/javascript"></script>
		<script type="text/javascript" charset="utf-8">
			$('.list').expList();

			$(document).ready(function() {
				$("#btn_search").click(function() {
					if(validate()) {
						// submitForm();
						$("#search_form").submit();
					}
					return false;
				});
			});
			
			//add to cart
			function addToCart(itemId){
				var qty = $("#qty_"+itemId).val();
				var values = $("#search_form").serializeObject();
				$.ajax({
					url : "cart_controller.php",
					type : "post",
					data : {"action":"add","id":itemId, "qty":qty},
					success : function(result) {
						var obj = $.parseJSON(result);
						if(obj['status'] == "SUCCESS") {
							alert(obj['message']);
							location.reload();
						} else {
							window.location.replace("login.php");
						}
					},
					error : function(result) {
						alert(result);
						// console.log(result)
					}
				});
			}
			
			//ajax submit form data
			function submitForm() {
				var values = $("#search_form").serializeObject();
				$.ajax({
					url : "search_controller.php",
					type : "post",
					data : values,
					success : function(result) {
						$('#bulletLooper').hide();
						var obj = $.parseJSON(result);
						alert(result);
						if(obj['status'] == "SUCCESS") {

						} else {
							//todo show no results
						}
						console.log(result)
					},
					error : function(result) {
						alert(result);
						console.log(result)
					}
				});
			};


			$.fn.serializeObject = function() {//serialize form data
				var o = {};
				var a = this.serializeArray();
				$.each(a, function() {
					if(o[this.name] !== undefined) {
						if(!o[this.name].push) {
							o[this.name] = [o[this.name]];
						}
						o[this.name].push(this.value || '');
					} else {
						o[this.name] = this.value || '';
					}
				});
				return o;
			};
			function validate() {//client side validation user inputs
				var search = $("#search_box").val();
				if(search == "") {
					return false;
				}
				return true;
			};
		</script>
		<link rel="stylesheet" href="css/looper.css" type="text/css" />
		<script src="js/looper.min.js" type="text/javascript" charset="utf-8"/>
<script type="text/javascript" charset="utf-8">
	jQuery(function($) {
		$('#bulletLooper').on('shown', function(e) {
			$('.looper-nav > li', this).removeClass('active').eq(e.relatedIndex).addClass('active');
		});
	});

		</script>
		<style type="text/css" media="screen">
			#bulletLooper nav img {
				margin-bottom: 8px;
			}
			#bulletLooper .item img {
				width: 100%;
				height: 100%;
				-webkit-box-shadow: inset 0 0 100px #fff;
				-moz-box-shadow: inset 0 0 100px #fff;
				box-shadow: inset 0 0 100px #fff;
			}
			@media only screen and (max-width: 767px) {
				#bulletLooper {
					display: none;
				}
			}
			#bulletLooper {
				margin-top: 20px;
				margin-bottom: 20px;
			}
			#search_form {
				background: -webkit-linear-gradient(#f9e5e6, #ec858f); /* For Safari 5.1 to 6.0 */
				background: -o-linear-gradient(#f9e5e6, #ec858f); /* For Opera 11.1 to 12.0 */
				background: -moz-linear-gradient(#f9e5e6, #ec858f); /* For Firefox 3.6 to 15 */
				background: linear-gradient(#f9e5e6, #ec858f); /* Standard syntax (must be last) */
				border: 1px solid #e03754;
				padding: 0px;
				border-radius: 10px;
				color: #000000;
				font-size: large;
				margin-top: 20px;
				margin-bottom: -50px;
				z-index: 99999;
				display: block;
				width: inherit;
			}
			#btn_search {
				border: none;
				height: 50px;
				border-left: 1px solid #e03754;
				cursor: pointer;
			}
			#search_box {
				background-color: transparent;
				border: none;
				font-size: large;
			}
			#section {
				min-height: 375px;
			}
			#results {
				padding-top: 75px;
				text-align: center;
			}
			.item {
				border: 1px solid #FFFFFF;
				width: 240px;
				min-height: 250px;
				display: inline-block;
				margin: 20px;
			}
			
			.item img{
				width: 100%;
				height: 200px;
			}
			#side {
				border: 2px solid #e03754;
				color: #000000;
				background-color: #FFFFFF;
				padding: 0px;
			}
			.list{
				width: 100%;
				margin: 0px;
				padding: 0px;
			}
			.list ul{
				margin: 0px;
				padding: 0px;				
			}
			.list li{
				width: 100%;
				display: block;
				cursor: pointer;
			}
			.list span{
				width: 100%;
				display: block;
				border: 1px solid #e03754;
				color: #000000;
				text-align: left;
				cursor: pointer;
				
			}
			.details{
				background-color: #791324;
				width: 100%;
				margin-top: 2px;
				margin-bottom: 2px;
			}
			
			.inline{
				display: inline-block
			}
			
			.btn_add{
				cursor: pointer;
			}
			.block{	
				display: block;
				border: 1 px solid black;
				padding: 0px;
				margin: 0px;
			}
			.item label{
				color: #fff;
			}
			
			#cart_summary{
				border: 2px solid #e03754;
				color: #000000;
				background-color: #FFFFFF;
				padding: 0px;
				min-height: 100px;
			}
			#btn_mycart{
				border: 2px solid #e03754;
				color: #fff;
				text-decoration:none;
				background-color: #791324;
				border-radius: 5px;
				padding: 10px;
				min-width: 50px;
			}
			
		</style>
	</body>
</html>
