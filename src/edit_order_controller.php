<?php
session_start();

require_once 'bootstrap.php';
require_once 'orm/dao/cart_dao.php';
require_once 'orm/dao/user_dao.php';
require_once 'orm/dao/item_dao.php';
require_once 'orm/dao/order_dao.php';
require_once 'orm/dao/cart_item_dao.php';

use model\CartItem;
use dao\UserDao;
use dao\ItemDao;
use dao\CartDao;
use dao\CartItemDao;
use dao\OrderDao;
use model\CartOrder;

if ($_POST) {
	$orderId = $_POST['id'];

	$cartDao = new CartDao($entityManager);
	$cartItemDao = new CartItemDao($entityManager);
	$userDao = new UserDao($entityManager);
	$orderDao = new OrderDao($entityManager);

	$order = $orderDao -> findById($orderId);
	if (!empty($order)) {
		$order -> setStatus("DELIVERED");
		$orderDao -> save($order);
	}

	$cart = $order->getCart();

	if (!empty($cart)) {
		$cartItems = $cartItemDao -> findByCart($cart);
		if (!empty($cartItems)) {
			foreach ($cartItems as $key => $cartItem) {
				$cartItem -> setStatus("DELIVERED");
				$cartItemDao->save($cartItem);
			}

			$_SESSION['message'] = "Order Updated";
			header('Location: orders.php');
		}
	}
}
?>